from flask import Blueprint, make_response as response, request
from functools import wraps
import simple_strop as ss


bp = Blueprint("views", __name__, url_prefix="")


def require_auth(fn):
    """When this decorator is applied to a view function, it will prevent the user from
    accessing the view without a valid authorization token defined in the header."""

    from .utils import token_exists

    @wraps(fn)
    def wrapped_fn(*args, **kwargs):
        if "Authorization" not in request.headers:
            return response(
                {
                    "message": (
                        'Unauthorized, supply "Authorization" header with a valid'
                        " token."
                    )
                },
                401,
            )
        elif not token_exists(request.headers["Authorization"]):
            return response({"message": "Invalid authorization token."}, 401)
        else:
            return fn(*args, **kwargs)

    return wrapped_fn


def record_history(fn):
    """When this decorator is applied to a view function, it will record the input,
    output, and user and save these details to the database in the History table."""

    from .utils import add_history

    @wraps(fn)
    def wrapped_fn(*args, **kwargs):
        res = fn(*args, **kwargs)
        if "output" in res.json:
            add_history(
                request.headers["Authorization"],
                request.path,
                request.get_json()["input"],
                res.json["output"],
            )
        return res

    return wrapped_fn


@bp.route("/", methods=["GET"])
def health_check():
    """Check that the server is running and can handle requests. The `bp.route` decorator
    is what tells flask to run this function when requests are received to the provided
    route. In this case, an empty path will execute this function so (when running on
    localhost) this view could be run by accessing http://localhost:5000/"""

    return response({"message": "System online."}, 200)


@bp.route("/register", methods=["POST"])
def register():
    """Create a new user account. This requires that the request json includes username
    and password fields that are populated with a username that doesn't already exist in
    the database and a password that is at least 8 characters long."""

    from .utils import create_user, user_exists

    data = request.get_json()

    if data is None:
        return response({"message": "Must supply arguments as json."}, 400)

    try:
        username = data["username"]
        password = data["password"]
    except KeyError:
        return response(
            {"message": "Username and password are required arguments."}, 400
        )
    else:
        if username == "" or password == "":
            return response(
                {"message": "Neither the username nor the password can be empty."}, 400
            )
        elif user_exists(username):
            return response({"message": "Username already taken."}, 400)
        elif len(password) < 8:
            return response({"message": "Password must be at least 8 characters."}, 400)

    user = create_user(username, password)

    if user is not None:
        return response(
            {"message": f'User "{user.username}" registered succesfully.'}, 200
        )
    else:  # pragma: no cover
        return response(
            {
                "message": (
                    "An unknown error occurred and we were unable to register "
                    "this user."
                )
            }
        )


@bp.route("/authenticate", methods=["POST"])
def authenticate():
    """Authenticate a user and get their authentication token. This view will check that
    the username and password provided in the request json are valid for some user and
    then return a token for that user via the `authenticate_user` function in utils.py."""

    from .utils import authenticate_user, user_exists

    data = request.get_json()

    try:
        username = data["username"]
        password = data["password"]
    except KeyError:
        return response({"message": "Username and password are required."}, 400)
    else:
        if username == "" or password == "":
            return response(
                {"message": "Neither the username nor the password can be empty."}, 400
            )
        elif not user_exists(username):
            return response({"message": "User does not exist."}, 400)

    token = authenticate_user(username, password)

    if token is not None:
        return response({"token": token})
    else:  # pragma: no cover
        return response(
            {
                "message": (
                    "An unknown error has occurred. You could not be authenticated."
                )
            }
        )


@bp.route("/reverse", methods=["POST"])
@require_auth
@record_history
def reverse():
    """Handle a request to call the simple_strop.reverse function on an input. Requires
    an input specified via the request json."""

    data = request.get_json()
    if "input" not in data:
        return response({"message": "Must specify an input in request json."}, 400)
    else:
        return response({"output": ss.reverse(data["input"])})


@bp.route("/piglatin", methods=["POST"])
@require_auth
@record_history
def piglatin():
    """Handle a request to call the simple_strop.piglatin function on an input. Requires
    an input specified via the request json."""

    data = request.get_json()
    if "input" not in data:
        return response({"message": "Must specify an input in request json."}, 400)
    else:
        return response({"output": ss.piglatin(data["input"])})


@bp.route("/get-history", methods=["GET"])
@require_auth
def get_history():
    """Return a user's history."""

    from .utils import get_user_from_token, get_serialized_history

    user = get_user_from_token(request.headers["Authorization"])
    history = get_serialized_history(user)

    return response({"history": history})
